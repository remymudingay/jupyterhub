.PHONY: help build tag push

TAG := $(shell git describe --always)
IMAGE := europeanspallationsource/jupyterhub

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  build       to build the Docker image"
	@echo "  tag         to tag the Docker image"
	@echo "  push        to push the Docker image"

build:
	docker build -t $(IMAGE):latest .

tag:
	docker tag $(IMAGE):latest $(IMAGE):$(TAG)

push:
	docker push $(IMAGE):$(TAG)
	docker push $(IMAGE):latest
