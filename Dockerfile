FROM jupyterhub/jupyterhub:2.1.0

LABEL MAINTAINER "benjamin.bertrand@esss.se"

# Install docker in the container
RUN wget https://get.docker.com -q -O /tmp/getdocker \
  && chmod +x /tmp/getdocker \
  && sh /tmp/getdocker \
  && rm -f /tmp/getdocker

# Install dockerspawner and ldapauthenticator
# Use a specific commit for ldapauthenticator (patch needed)
RUN /opt/conda/bin/pip install --no-cache-dir \
    dockerspawner==0.11.1 \
    jupyterhub-ldapauthenticator==1.3.0
