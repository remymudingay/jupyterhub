# Configuration file for JupyterHub
import os
from tornado import gen
from ldapauthenticator import LDAPAuthenticator


class LDAPAuthenticatorAuthState(LDAPAuthenticator):
    @gen.coroutine
    def pre_spawn_start(self, user, spawner):
        auth_state = yield user.get_auth_state()
        self.log.debug(f"pre_spawn_start user: {user} auth_state: {auth_state}")
        if not auth_state:
            return
        # setup environment
        spawner.environment["NB_UID"] = str(auth_state["uidNumber"][0])
        spawner.environment["NB_GID"] = str(auth_state["gidNumber"][0])
        spawner.environment["NB_USER"] = auth_state["uid"][0]
        spawner.environment["NB_GROUP"] = "DomainUsers"
        spawner.environment["NB_HOME"] = auth_state["homeDirectory"][0]


c.Spawner.default_url = "/lab"
# Spawn single-user servers as Docker containers
c.JupyterHub.spawner_class = "docker"
# Spawn containers from this image
c.DockerSpawner.image = "registry.esss.lu.se/ics-docker/notebook"
# JupyterHub requires a single-user instance of the Notebook server, so we
# default to using the `start-singleuser.sh` script included in the
# jupyter/docker-stacks *-notebook images as the Docker run command when
# spawning containers.
spawn_cmd = "start-singleuser.sh"
c.DockerSpawner.extra_create_kwargs.update({"command": spawn_cmd})
# Connect containers to this Docker network
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = "jupyterhub-network"
# Pass the network name as argument to spawned containers
c.DockerSpawner.extra_host_config = {"network_mode": "jupyterhub-network"}
# Explicitly set notebook directory because we'll be mounting a host volume to
# it.  Most jupyter/docker-stacks *-notebook images run the Notebook server as
# user `jovyan`, and set the notebook directory to `/home/jovyan/work`.
# We follow the same convention.
notebook_dir = "/home/conda/notebooks"
c.DockerSpawner.notebook_dir = "/home/conda/notebooks"
# Remove containers once they are stopped
c.DockerSpawner.remove_containers = True
# For debugging arguments passed to spawned containers
c.DockerSpawner.debug = True

# User containers will access hub by container name on the Docker network
c.JupyterHub.hub_ip = "jupyterhub"
c.JupyterHub.hub_port = 8080

# Port used by reverse proxy to access Jupyterhub
c.JupyterHub.port = 8000

c.JupyterHub.authenticator_class = LDAPAuthenticatorAuthState

# Authenticate users with LDAP
c.LDAPAuthenticatorAuthState.server_address = "ldap-cslab.cslab.esss.lu.se"
c.LDAPAuthenticatorAuthState.use_ssl = True
c.LDAPAuthenticatorAuthState.bind_dn_template = [
    "uid={username},ou=Users,dc=esss,dc=lu,dc=se"
]
c.LDAPAuthenticatorAuthState.lookup_dn = False
c.LDAPAuthenticatorAuthState.allowed_groups = []
c.LDAPAuthenticatorAuthState.escape_userdn = False
c.LDAPAuthenticatorAuthState.auth_state_attributes = [
    "uid",
    "uidNumber",
    "gidNumber",
    "homeDirectory",
]
c.LDAPAuthenticatorAuthState.debug = True
c.LDAPAuthenticatorAuthState.enable_auth_state = True

# Persist hub data on volume mounted inside container
# c.JupyterHub.db_url = 'sqlite:////data/jupyterhub.sqlite'
# c.JupyterHub.cookie_secret_file = '/data/jupyterhub_cookie_secret'

c.Authenticator.admin_users = {"benjaminbertrand"}
c.JupyterHub.admin_access = False
