# Configuration file for JupyterHub

c.Spawner.default_url = "/lab"
# Spawn single-user servers as Docker containers
c.JupyterHub.spawner_class = "docker"
# Spawn containers from this image
c.DockerSpawner.image = "registry.esss.lu.se/ics-docker/notebook"
# JupyterHub requires a single-user instance of the Notebook server, so we
# default to using the `start-singleuser.sh` script included in the
# jupyter/docker-stacks *-notebook images as the Docker run command when
# spawning containers.
spawn_cmd = "start-singleuser.sh"
c.DockerSpawner.extra_create_kwargs.update({"command": spawn_cmd})
# Connect containers to this Docker network
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = "jupyterhub-network"
# Pass the network name as argument to spawned containers
c.DockerSpawner.extra_host_config = {"network_mode": "jupyterhub-network"}
# Explicitly set notebook directory because we'll be mounting a host volume to
# it.  Most jupyter/docker-stacks *-notebook images run the Notebook server as
# user `jovyan`, and set the notebook directory to `/home/jovyan/work`.
# We follow the same convention.
notebook_dir = "/home/conda/notebooks"
c.DockerSpawner.notebook_dir = "/home/conda/notebooks"
# Remove containers once they are stopped
c.DockerSpawner.remove_containers = True
# For debugging arguments passed to spawned containers
c.DockerSpawner.debug = True

# User containers will access hub by container name on the Docker network
c.JupyterHub.hub_ip = "jupyterhub"
c.JupyterHub.hub_port = 8080

# Port used by reverse proxy to access Jupyterhub
c.JupyterHub.port = 8000

c.JupyterHub.authenticator_class = "dummy"
c.DummyAuthenticator.password = "password"


# Persist hub data on volume mounted inside container
# c.JupyterHub.db_url = 'sqlite:////data/jupyterhub.sqlite'
# c.JupyterHub.cookie_secret_file = '/data/jupyterhub_cookie_secret'

c.Authenticator.admin_users = {"benjaminbertrand"}
c.JupyterHub.admin_access = False
