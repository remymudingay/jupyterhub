# jupyterhub ESS Docker image

This Dockerfile creates a [Docker](https://www.docker.com) image based on the official `jupyterhub/jupyterhub`.

It adds docker engine, dockerspawner and ldapauthenticator.

## Testing

A local docker-compose.yml file allows testing of both the Dummy and LDAP authenticator.

Create a `.env` file with the following variables:

```
LDAP_BIND_USER_DN=<ldap user>
LDAP_BIND_USER_PASSWORD=<ldap password>
JUPYTERHUB_CRYPT_KEY=<key>
```

Then just run `docker-compose up` and try to login.

The default config is for ICS LDAP.
To test the dummy authenticator, run:

`docker-compose -f docker-compose.dummy.yml up`

IT AD is deprecated (LDAP is required to get the user uid/gid).
It can still be tested with:

`docker-compose -f docker-compose.ad.yml up`
